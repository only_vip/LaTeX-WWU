%!TEX root = ../MC_SS17.tex
\section{Temporale Logik}
\label{sec:para3}

Viele Anforderungen an Systeme betreffen das dynamische Verhalten des betrachteten Systems.

\begin{bsp_no}[Fahrstuhl]
	\begin{enumerate}[1)]
		\item Jede Fahrstuhlanforderung muss schließlich bedient werden.
		\item Der Fahrstuhl fährt nie ohne Anzuhalten an einem Stock vorbei, wenn er dort angefordert wurde.
	\end{enumerate}
\end{bsp_no}

Anforderungen könnten in Prädikatenlogik formalisiert werden.

\begin{bsp_no}
	\begin{description}
		\item[$H(t)$] Fahrstuhlposition zum Zeitpunkt $t$
		\item[$req(n, t)$] Fahrstuhl ist zum Zeitpunkt $t$ im $n$-ten Stock angefordert.
		\item[$serve(n, t)$] Fahrstuhl hält zum Zeitpunkt $t$ im $n$-ten Stock.
	\end{description}

	Angewandt auf die Anforderungen:
	\begin{enumerate}[1)]
		\item $\forall t \forall n: (req(n,t) \Rightarrow \exists t' \geq t: serve(n, t'))$
		\item $\forall t \forall t' > t \forall n: \left( (req(n, t) \wedge H(t') \neq n \wedge \exists t_{trav}: t \leq t_{trav} \wedge H(t_{trav}) = n ) \Rightarrow \exists t_{serv}: t \leq t_{serv} \leq t' \wedge serv(n, t_{serve})\right)$
	\end{enumerate}
\end{bsp_no}

Der Vorteil an der Prädikatenlogik: Sie ist sehr exakt und beseitigt Missverständlichkeiten der informellen Spezifikation. Jedoch ist sie ziemlich unübersichtlich.

\subsection*{Temporale Logiken}
\begin{itemize}
	\item Zeitpunkte sind implizit (keine Variable $t$ in Formeln)
	\item Temporale Konzepte wie \enquote{immer}, \enquote{schließlich}, \enquote{nie} stehen direkt zur Verfügung
\end{itemize}

Es gibt zwei Typen temporaler Logiken:
\begin{itemize}
	\item Linear-time Logiken: Eigenschaften von Berechnungspfaden, Traces und Läufen, wie im vorherigen Kapitel betrachtet.
	
	\item Branching-time Logiken: Eigenschaften des Berechnungsbaum

	\begin{center}
		\todo{Bild einfügen}
	\end{center}
\end{itemize}

\begin{bsp_no}
	\mbox{}
	\begin{center}
		\todo{Bild einfügen}
	\end{center}

	Die branching-time Logik kann den Unterschied erkennen, die linear-time Logik hingegen nicht.
\end{bsp_no}

\subsection{Linear Time Logik}

Den Ursprung hat die Linear Time Logik in der \enquote{Tense Logic} von Prior (1967). Sie basiert auf vier Modalitäten:
\begin{description}
	\item[$F$] \enquote{es wird} (Future, \underline{F}inally)
	\item[$P$] \enquote{es war} (\underline{P}ast)
	\item[$X$] \enquote{im nächsten Moment} (ne\underline{x}t)
	\item[$Y$] \enquote{im vorherigen Moment} (\underline{Y}esterday)
\end{description}

Sei $AP$ eine Menge atomarer Propositionen.

\begin{defn}[Tense-Logik]
	Eine Formel der \emph{Tense-Logik} ist ein Ausdruck, der sich gemäßg der folgenden (abstrakten) Grammatik bilden lässt.
	\[
		\phi ::= p \mid \phi_1 \land \phi_2 \mid \lnot \phi \mid F\phi \mid P \phi \mid X \phi \mid Y \phi
	\]
	Dabei steht $p$ für atomare Propositionen aus $AP$.
\end{defn}

\begin{bem}
	$\{\land, \lnot\}$ ist eine Operatorbasis. Deshalb lassen sich die übrigen boolschen Operatoren als Abkürzung einführen, z.B.
	\[
		\phi_1 \lor \phi_2 \equiv_{def} \lnot(\lnot \phi_1 \land \lnot \phi_2)
		\text{  oder  }
		\phi_1 \Rightarrow \phi_2 \equiv_{def} \not \phi_1 \lor \phi_2
	\]
	Wir verwenden deshalb beliebige boolesche Operatoren in Formeln (auch in später eingeführten Logiken).
\end{bem}

\begin{bem}
	Die Grammatik ist eine sogenannte \emph{abstrakte} Grammatik, die die Baumstruktur von Formeln beschreibt. Wir verwenden wie üblich Klammern, um hingeschriebene Formeln eindeutig parsen zu können.
\end{bem}

\begin{bsp}
	\begin{enumerate}[1)]
		\item \textit{$\F$ rain}:  es wird regnen
		\item \textit{$\lnot \F$ rain}:  es wird nie regnen
		\item \textit{$\F \lnot$ rain}: es wird irgendwann mal nicht regnen
		\item \textit{$\F$ rain $\land \F \lnot$ rain} es wird regnen und es wird nicht regnen
		\item \textit{rain $\Rightarrow \F \Past$ rain} wenn es jetzt regnet, dann wird es geregnet haben
		\item \textit{$\lnot \F \lnot$ rain} es wird immer regnen
		\item \textit{$\Past($rain $\land \lnot \X$ rain$)$} es gab eine Zeit da es regnete, aber dann aufhörte zu regnen
	\end{enumerate}
\end{bsp}

\begin{bem}
	Gewisse weitere Modalitäten lassen sich als Abkürzungen darstellen, z.B.
	\begin{enumerate}[a)]
		\item $\G \phi \equiv_{def} \lnot \F \lnot \phi$ \enquote{es wird immer $\phi$}
		\item $\Hist \phi \equiv_{def} \lnot \Past \lnot \phi$ \enquote{es war immer $\phi$}
		\item $\alw \phi \equiv_{def} \G \F \phi$ \enquote{$\phi$ wird unendlich oft gelten}
		\item $\fin \phi \equiv_{def} \F \G \phi$ \enquote{ab irgendwann wird $\phi$ immer wahr sein}
	\end{enumerate}
\end{bem}

\begin{bsp}
	$\alw \textit{ rain} \lor \fin \textit{ sun}$ \enquote{es wird immer wieder Regnen oder ab irgendwann wird immer die Sonne scheinen.}
\end{bsp}

Im Folgenden beschränken wir uns auf Zukunftsformeln, ohne Modalitäten $\Past, \Y$ und daraus abgeleitete Modalitäten.

\begin{defn}[Totale Kripke-Strukturen]
	Eine Kripke-Struktur $\kts = (S, S_0, R, L)$ heißt \emph{total}, gdw. jeder Zustand einen Nachfolger hat.
	\[
		\forall s \in S \exists s' \in S: (s, s') \in R
	\]
\end{defn}

\begin{bem}
	Man kann Kripke-Strukturen leicht künstlich total machen, indem man z.B. einen neuen Zustand hinzufügt, der Terminierung anzeigt:
	\[
		\kts = (S, S_0, R, L) \rightsquigarrow \kts_{total} = (S_{tot}, S_0, R_{tot}, L_{tot})
	\]
	mit
	\begin{itemize}
		\item $S_{tot} = S \dot{\cup} \{t\}$
		\item $R_{tot} = R \cup \{(s,t) \mid \nexists s' \in S: (s, s') \in R\} \cup \{(t,t)\}$
		\item $L_{tot}: S \rightarrow AP, L_{tot} = 
		\begin{cases}
			L(s), & \text{für } s \in S\\    
			\{\textit{term}\}, & \text{für } s = t
		\end{cases}$
	\end{itemize}

	\begin{center}
		\begin{tikzpicture}
			\node[]() at (-2, -0.5) {$\mathcal{K}$};
			\node[state, initial, label = {[align=left, right, xshift=3mm]P,Q}](1) at (0,-2){1};
			\node[state](2) at (-1,-4){2};
			\node[state, label = {[align=left, xshift=2mm, right]P}](3) at (1,-4){3};
			
			\path[->]
			(1) edge [bend left = 15pt] (2)
			(1) edge [] (3)
			(2) edge [bend left = 15pt] (1)
			(2) edge [] (3);
			
			\node[]() at (6, -0.5) {$\mathcal{K}_{tot}$};
			\node[state, initial, label = {[align=left, right, xshift=3mm]P,Q}](1) at (8,-2){1};
			\node[state](2) at (7,-4){2};
			\node[state, label = {[align=left, xshift=2mm, right]P}](3) at (9,-4){3};
			\node[state, label = {[align=left, xshift=2mm, right]term}](4) at (11,-4){t};
			
			\path[->]
			(1) edge [bend left = 15pt] (2)
			(1) edge [] (3)
			(2) edge [bend left = 15pt] (1)
			(2) edge [] (3)
			(3) edge [] (4);
			
			\node[]() at (4, -2.8) {$\rightsquigarrow$};
		\end{tikzpicture}
	\end{center}
\end{bem}

\begin{center}
	\framebox{In Zukunft sind Kripke-Strukturen immer total!!}
\end{center}

Sei $\kts = (S, S_0, R, L)$ eine (totale) Kripke-Struktur.

\begin{defn}[Pfad, Ausführung]
	\begin{enumerate}[a)]
		\item Ein \emph{Pfad} ist eine unendliche Folge $\pi = s_0, s_1, s_2, \dots$ von Zuständen $s_i \in S$, so dass $(s_i, s_{i+1}) \in R\ \forall i \in \mathbb{N}_0$.
		\item Eine \emph{Ausführung} ist ein Pfad $\pi = s_0, s_1,  \dots$ mit $s_0 \in S_0$.
		\item Für eine unendliche Folge von Zuständen $\pi = s_0, s_1, s_2, \dots$ sei
		\begin{itemize}
			\item $\pi(i)$ der $i$-te Zustand $s_i$
			\item $\pi^i$ der Suffix von $\pi$ ab $s_i$, d.h. $\pi^i = s_i, s_{i+1}, s_{i+2}, \dots$
		\end{itemize}
		für $i \in \mathbb{N}_0$.
	\end{enumerate}
\end{defn}

\begin{defn}[Erfüllen von Zukunftsformeln]
	\begin{enumerate}[a)]
		\item Wir definieren induktiv, wann eine Folge von Zuständen $\pi \in S^\omega$ eine Zukunftsformel $\phi$ der Tense-Logik erfüllt:
		
		\begin{tabular}{ll}
			$\pi \models p$ & gdw. $p \in L(\pi(0))$\\
			$\pi \models \phi_1 \land \phi_2$ & gdw. $\pi \models \phi_1$ und $\pi \models \phi_2$\\
			$\pi \models \lnot \phi$ & gdw. nicht $\pi \models \phi$\\
			$\pi \models \F \phi$ & gdw. $\exists j \geq 0: \pi^j \models \phi$\\
			$\pi \models \X \phi$ & gdw. $\pi^1 \models \phi$
		\end{tabular}
	
		\item Für $s \in S: s \models \phi$ gdw. für alle Pfade $\pi$ mit $\pi(0) =s$ gilt: $\pi \models \phi$
		
		\item $\kts \models \phi$ gdw. für alle Ausführungen $\pi$ von $\kts$ gilt: $\pi \models \phi$ (gdw. $\forall s \in S_0: s \models \phi$).
	\end{enumerate}
\end{defn}

\begin{bsp}
	\mbox{}
	
		\begin{minipage}{0.5\textwidth}
			\begin{tikzpicture}
				\node[]() at (-2, -0.5) {$\mathcal{K}$};
				\node[state, initial](1) at (-1,-2){1};
				\node[state, label = {[align=left, xshift=2mm, right]Q}](2) at (-1,-4){2};
				\node[state, label = {[align=left, xshift=2mm, right]R}](3) at (1,-4){3};
				
				\path[->]
				(1) edge [bend left = 15pt] (2)
				(2) edge [bend left = 15pt] (1)
				(3) edge [loop right] (3)
				(2) edge [] (3);
			\end{tikzpicture}
			\begin{itemize}
				\item $\kts \models \X Q$
				\item $\kts \not{\models} \F R$
				\item $\kts \not{\models} \lnot \F R$
			\end{itemize}
		\end{minipage}
		\begin{minipage}{0.5\textwidth}
			\begin{itemize}
				\item[] Ausführungen $\pi = 1, 2, 1, 2, 3, 3, \dots$
				\item $\pi \models \X Q$
				\item $\pi \models \F R$
				\item $\pi \not{\models} \lnot \F R$
				\item[] Ausführungen $\pi' = 1,2,1,2,1,2, \dots$
				\item $\pi' \models \X Q$
				\item $\pi' \not{\models} \F R$
				\item $\pi' \not{\models} \lnot \F R$
			\end{itemize}
		\end{minipage}
\end{bsp}

Es gibt verschiedene Arten von Problemen:
\begin{description}
	\item[Erfüllbarkeitsproblem] Entscheide für Formel $\phi: \exists \kts \exists \pi: \pi \models \phi$? Beispiel: Die Formel $p \land \lnot p$ ist nicht erfüllbar.
	
	\item[Allgemeingültigkeitsproblem] Entscheide für Formel $\phi$: $\forall \kts \forall \pi: \pi \models \phi$? Beispiel: Die Formel $p \lor \lnot p$ ist allgemeingültig.
	
	\item[Model-Checking-Problem] Entscheide für Formel $\phi$ und Kripke-Struktur $\kts$, ob $\kts \models \phi$.
\end{description}

Man kann weitere Modalitäten einführen, die es erlauben, weitere Eigenschaften zu spezifizieren:
\begin{itemize}
	\item \enquote{Until} $\U$
	\begin{itemize}
		\item Syntax: $\phi ::= \phi_1 \U \phi_2$
		\item Semantik: $\pi \models \phi_1 \U \phi_2$ gdw. $\exists k \geq 0 (\pi^k \models \phi_2 \land \forall j, 0 \leq j < k: \pi^j \models \phi_1)$
		\item \todo{Bild einfügen}
	\end{itemize}
	\item \enquote{Weak Until} $\W$
	\begin{itemize}
		\item Syntax: $\phi ::= \phi_1 \W \phi_2$
		\item Semantik: $\pi \models \phi_1 \W \phi_2$ gdw. $\exists k \geq 0 (\pi^k \models \phi_2 \land \forall j, 0 \leq j < k: \pi^j \models \phi_1)$ oder $\forall j \geq 0: \pi^j \models \phi_1$
		\item \todo{Bild einfügen}
	\end{itemize}
	\item \enquote{Release} $\R$
	\begin{itemize}
		\item Syntax: $\phi ::= \phi_1 \R \phi_2$
		\item Semantik: $\pi \models \phi_1 \R \phi_2$ gdw. $\forall k \geq 0: \pi^k \models \lnot \phi_2 \Rightarrow \exists j < k: \pi^j \models \phi_1$
		\item[] ($\phi_1$ erlöst $\phi_2$ von der Verpflichtung zu gelten)
		\item \todo{Bild einfügen}
	\end{itemize}
\end{itemize}

\begin{defn}[Äquivalenz von LTL-Formeln]
	Zwei Formeln $\varphi$ und $\psi$ heißen \emph{äquivalent} in Zeichen $\varphi \equiv \psi$, gdw.
	\[
		\pi \models \varphi \Leftrightarrow \pi \models \psi
	\]
	für alle Pfade $\pi$ in allen Kripke-Strukturen erfüllt ist.
\end{defn}

\begin{bem}
	\label{bem:dualitäten}
	Einige Modalitäten lassen sich durch andere Modalitäten ausdrücken, z.B.:
	\begin{enumerate}[a)]
		\item $\F \phi \equiv \texttt{true} \U \phi$
		\item[] Sei $\pi$ ein beliebiger Pfad in einer beliebigen Kripke-Struktur.
		\begin{align*}
			& \pi \models \texttt{true} \U \phi \\
			\overset{Def.}{\iff} & \exists k \geq 0: \pi^k \models \phi \land \underbrace{\forall j, 0 \leq 0 < k: \underbrace{\pi^j \models \texttt{true}}_{\text{immer wahr}}}_{\text{immer wahr}} \\
			\overset{\text{PÄ}}{\iff} & \exists k \geq 0: \pi^k \models \phi \\
			\overset{Def. \F}{\iff} & \pi \models \F \phi
		\end{align*}
		\item $\G \phi \overset{Def.}{\equiv} \lnot \F \lnot \phi \overset{a)}{\equiv} \lnot (\texttt{true} \U \lnot \phi)$
		
		\item $\F \phi \equiv \lnot \G \lnot \phi$
		
		\item $\varphi \W \psi \equiv \varphi \U \psi \lor \G \varphi$
		
		\item $\varphi R \psi \equiv \lnot (\lnot \varphi \U \lnot \psi)$
		\item[] Sei $\pi$ ein beliebiger Pfad in einer beliebigen Kripke-Struktur.
		\begin{align*}
			& \pi \models \lnot( (\lnot \varphi) \U (\lnot \psi)) \\
			\overset{Def.}{\iff} & \text{nicht } \pi \models (\lnot \varphi) \U (\lnot \psi)\\
			\overset{Def. \U}{\iff} & \text{nicht } \exists k \geq 0: (\pi^k \models \lnot \psi \land \forall j, 0 \leq j < k: \pi^j \models \lnot \varphi)\\
			\overset{\text{PÄ}}{\iff} & \forall k \geq 0: (\text{nicht } \pi^k \models \lnot \psi) \lor \underbrace{\text{nicht } \forall j, 0 \leq j < k: \pi^j \models \lnot \varphi}_{\exists j, 0 \leq j < k \underbrace{\text{nicht } \pi^i \models \varphi}_{\pi^j \models \varphi}} \\
			\iff & \forall k \geq 0: \pi^k \models \lnot \psi \Rightarrow \exists j, 0 \leq j < k: \pi^i \models \varphi \\
			\overset{Def. \R}{\iff} \pi \models \varphi \R \psi
		\end{align*}
		\item $\varphi \U \psi \equiv \lnot ( (\lnot \varphi) \R (\lnot \psi))$
	\end{enumerate}
\end{bem}

Frage: Was ist eine gute Auswahl von Modalitäten?

\begin{defn}[LTL]
	Wir bezeichen die Lineare temporale Logik mit Modalitäten $Op_1, \dots, Op_n$ als $L(Op_1, \dots, Op_n)$. Zum Beispiel ist $\lxu$ die temporale Logik mit den Formeln
	\[
		\phi ::= p \mid \phi_1 \land \phi_2 \mid \lnot \phi \mid \X \phi \mid \phi_1 \U \phi_2
	\]
	$L(\X,\U)$ wird auch als \emph{LTL} bezeichnet.
\end{defn}

Da wir alle übrigen Zukunftsmodalitäten wie $\F, \G, \W, \R$ durch $\U$ ausdrücken können, erlauben wir auch diese in LTL-Formeln.

\begin{defn}[Ausdrucksstärke von Logiken]
	\begin{enumerate}[a)]
		\item Eine Logik $L'$ heißt \emph{mindestens so ausdrucksstark} wie eine Logik $L$, in Zeichen $L \leq L'$, wenn gilt:
		\[
			\forall \phi \in L: \exists \phi' \in L': \phi \equiv\phi'
		\]
		
		\item $L$ und $L'$ sind von gleicher Ausdrucksstärke, in Zeichen $L \equiv L'$, wenn $L \leq L'$ und $L' \leq L$.
		
		\item $L'$ heißt echt ausdruckstärker als $L$, in Zeichen $L < L'$, wenn $L \leq L'$ gilt, aber nicht $L' \leq L$. Es gilt dann insbesondere:
		\[
			\exists \phi' \in L': \exists \phi \in L: \phi \equiv \phi'
		\]
	\end{enumerate}
\end{defn}

\begin{bem}
	\label{bem:mod-ausdruckskraft}
	Offensichtlich kann durch Hinzufügen von Modalitäten die Ausdruckskraft nicht sinken. Zusammen mit den Beziehungen aus Bemerkung \ref{bem:dualitäten} folgen daraus eine Reihe von Beziehungen zwischen Logiken:
	\begin{enumerate}[a)]
		\item $L(\G) \equiv L(\F) \leq L(\U) \equiv L(\U, \F, \G, \W, \R) [\equiv L(\R) \equiv L(\W)]$
		\item $L(\X) \leq L(\X, \G) \equiv L(\X, \F) \leq \lxu \equiv L(\X, \U, \F, \G, \W, \R) [\equiv L(\X, \R) \equiv L(\X, \W)]$
		\item $L(\U) \leq \lxu$
	\end{enumerate}
\end{bem}

\begin{satz}
	\[L(\U) < \lxu\]
\end{satz}

\begin{beweis}
	Sei $\pi$ die eindeutige Ausführung der Kripke-Struktur
	\begin{center}
		\begin{tikzpicture}
			\node[state, initial, minimum size=5mm, label = {[align=center, below, yshift=-5mm]Q}](1) at (0,0) {\footnotesize 1};
			\node[state, minimum size=5mm, label = {[align=center, below, yshift=-5mm]Q}](2) at (1,0) {\footnotesize 2};
			\node[state, minimum size=5mm](3) at (2,0) {\footnotesize 3};
			
			\path[->]
				(1) [] edge (2)
				(2) [] edge (3)
				(3) [loop right] edge (3);
		\end{tikzpicture}
	\end{center}

	Offensichtlich gilt für $\X Q \in \lxu$: $\pi \models \X Q$ und $\pi^1 \not{\models} \X Q$.
	
	Wir zeigen nun durch strukturelle Induktion:
	\[
		\forall \phi \in L(\U): \pi \models \phi \Leftrightarrow \pi^1 \models \phi
	\]
	
	Sei $\phi \in L(\U)$
	
	\underline{Induktionsannahme:} Für alle echten Teilformeln $\psi$ von $\phi$ gelte $\pi \models \psi \Leftrightarrow \pi^1 \models \psi$.
	
	\underline{Fall 1:} $\phi = p$. Die Behauptung gilt, da die Zustände $1$ und $2$ die gleichen atomaren Propositionen erfüllen.
	
	\underline{Fall 2:} $\phi = \phi_1 \land \phi_2$.
	\[
			\pi \models \phi_1 \land \phi_2 
	\underset{\text{Definition von } \models}{\iff}
	\pi \models \phi_1 \text{ und } \pi \models \phi_2 
	\underset{\text{Induktionsannahme}}{\iff}
	\pi^1 \models \phi_1 \text{ und } \pi^1 \models \phi_2 
	\underset{\text{Definition von } \models}{\iff}
	\pi^1 \models \phi_1 \land \phi_2
	\]
	
	\underline{Fall 3:} $\phi = \lnot \phi_1$. Folgt analog zu Fall 2.
	
	\underline{Fall 4:} $\phi = \phi_1 \U \phi_2$
	
	\enquote{$\Rightarrow$}: Gelte $\pi = \phi_1 \U \phi_2$. Dann muss einer der folgenden drei Fälle eintreten:
	\begin{enumerate}[a)]
		\item $\pi^0 \models \phi_1, \pi^1 \models \phi_1, \pi^2 \models \phi_2$
		\item[] $\Rightarrow \pi^1 \models \phi_1 \U \phi_2$
		\item $\pi^0 \models \phi_1, \pi^1 \models \phi_2$
		\item[] $\Rightarrow \pi^1 \models \phi_2 \U \phi_2$
		\item $\pi^0 \models \phi_2$
		\item[] $\Rightarrow \pi^1 \models \phi_2$ (nach Induktionsannahme)
		\item[] $\Rightarrow \pi^1 \models \phi_1 \U \phi_2$
	\end{enumerate}

	\enquote{$\Leftarrow$}: Gelte $\pi^1 \models \phi_1 \U \phi_2$. Dann muss einer der beiden folgenden Fälle eintreten:
	\begin{enumerate}[a)]
		\item $\pi^1 \models \phi_1, \pi^2 \models \phi_2$
		\item[] $\Rightarrow \pi^0 \models \phi_1$ (nach Induktionsannahme)
		\item[] $\Rightarrow \pi \models \phi_1 \U \phi_2$
		\item $\pi^1 \models \phi_2$
		\item[] $\Rightarrow \pi \models \phi_2$ (nach Induktionsannahme)
		\item[] $\Rightarrow \pi \models \phi_1 \U \phi_2$ (nach Definition von $\models$ für $\U$)
	\end{enumerate}
	
	Wir haben die Aussage für alle vier Fälle gezeigt, also kann es keine zu $\X Q$ äquivalente Formel in $L(\U)$ geben.
\end{beweis}

Wir wollen als nächstes die Aussage $L(\X, \F) < \lxu$ zeigen.

\begin{defn}
	Für $i \geq 0$ sei:
	\[
		L(\X^i, \F) = \{\phi \in L(\X, \F) \mid \phi \text{ enthält höchstens } i \text{ Vorkommen von } \X\}
	\]
	und analog
	\[
		L(\X^i, \U) = \{\phi \in \lxu \mid \phi \text{ enthält höchstens } i \text{ Vorkommen von } \X\}
	\]
\end{defn}

\begin{bem}
	Wir können leicht sehen, dass die Aussagen
	\begin{itemize}
		\item $L(\X^0, \F) \leq L(\X^1, \F) \leq L(\X^2, \F), \dots$
		\item $L(\X, \F) = \bigcup^{\infty}_{i=0} L(\X^i, \F)$
	\end{itemize}
	gelten.
\end{bem}

\begin{defn}
	Für $m \geq 0$ sei $\kts_m$ die folgende Kripke-Struktur:
	\begin{center}
		\begin{tikzpicture}
			\node[state, initial, minimum size=10mm](sm) at (0,0) {\footnotesize $s_m$};
			\node[state, minimum size=10mm](sm1) at (2,0) {\footnotesize $s_{m-1}$};
			\node[minimum size=10mm](sdots) at (4,0) {\dots};
			\node[state, minimum size=10mm](s1) at (6,0) {\footnotesize $s_{1}$};
			\node[state, minimum size=10mm](s0) at (8,0) {\footnotesize $s_{0}$};
			\node[state, minimum size=10mm, label = {[align=center, below, yshift=-10mm]Q}](q) at (10,0) {\footnotesize $q$};
			
			\node[state, minimum size=10mm](tm) at (0,-3) {\footnotesize $t_m$};
			\node[state, minimum size=10mm](tm1) at (2,-3) {\footnotesize $t_{m-1}$};
			\node[minimum size=10mm](tdots) at (4,-3) {\dots};
			\node[state, minimum size=10mm](t1) at (6,-3) {\footnotesize $t_{1}$};
			\node[state, minimum size=10mm](t0) at (8,-3) {\footnotesize $t_{0}$};
			\node[state, minimum size=10mm, label = {[align=center, below, yshift=5mm]P}](p) at (10,-3) {\footnotesize $p$};
			
			\path[->]
				(sm) edge[] (sm1)
				(sm1) edge[] (sdots)
				(sdots) edge[] (s1)
				(s1) edge[] (s0)
				(s0) edge[] (q)			
				(tm) edge[] (tm1)
				(tm1) edge[] (tdots)
				(tdots) edge[] (t1)
				(t1) edge[] (t0)
				(t0) edge[] (p)
				(q) edge[] (tm)
				(p) edge[] (sm);		
		\end{tikzpicture}
	\end{center}
	
	Zur Vereinfachung bezeichnen wir den eindeutig bestimmten Pfad vom Zustand $r$ im Folgenden mit $r$.
\end{defn}

\begin{lemma}
	\label{lemma:ks_f_konstruktion}
	Für alle $\phi \in L(\X, \F)$ und alle $i \in \{0, \dots, m\}$ gilt in $\kts_m$:
	\[
		\phi \in L(\X^i, \F) \Rightarrow s_i \models \phi \Leftrightarrow t_i \models \phi
	\]
\end{lemma}

\begin{beweis}
	Wir zeigen das Lemma über eine strukturelle Induktion über Konstruktion von $\phi$.
	
	Induktionsannahme: Sei also $\phi \in L(\X^i, \F)$. Für alle echten Teilformeln von $\phi$ gelte die Behauptung.
	
	Ist $\phi$ von der Form $p \in AP$, so folgt die Behauptung durch Anschauen von $\kts_m$; Ist $\phi$ von der Form $\lnot \phi_1$ oder $\phi_1 \land \phi_2$ so folgt die Behauptung direkt aus der Induktionsannahme.
	
	\underline{Fall:} $\phi = \X \psi$: Wegen $\phi \in L(\X^i, \F)$ ist dann $\psi \in L(\X^{i-1}, \F).$ Dann gilt:
	\[
		s_i \models \phi 
		\iff  s_i \models \X \psi
		\underset{\text{Bed. von }\X, \text{Nf.}}{\iff} s_i \models \psi
		\underset{\text{Ind.-Ann.}}{\iff} t_{i-1} \models \psi
		\underset{\text{Bed. von}\X, \text{Nf.}}{\iff} t_i \models \X \psi
		\iff t_i \models \phi
	\]
	wobei wir verwenden, dass $s_{i-1}$ bzw. $t_{i-1}$ eindeutiger Nachfolger von $s_i$ bzw. $t_i$ ist (Nf.).
	
	\underline{Fall:} $\phi = \F \psi$: Wegen der zyklischen Form von $\kts_m$ gilt:
	\[
		s_i \models \phi
		\underset{\text{Def. von } \models \text{ für } \F}{\iff} \exists \text{ Zustand } r: r \models \psi
		\underset{\text{Def. von } \models \text{ für } \F}{\iff} t_i \underbrace{\F \psi}_{\phi}
	\]
	
\end{beweis}

\begin{satz}[Kamp, 1968]	
	\[
		L(\X, \F) < \lxu
	\]
\end{satz}

\begin{beweis}
	Wir wissen, dass $L(\X, \F) \leq \lxu$ nach Bemerkung \ref{bem:mod-ausdruckskraft} gilt. Nun wollen wir zeigen, dass $\lnot P \U Q$ durch keine Formel in $L(\X, \F)$ ausgedrückt werden kann.
	
	Annahme: $\lnot P \U Q \equiv \phi$ für ein $\phi \in L(\X, \F)$.
	
	Wähle $m$ mit $\phi \in L(\X^m, \U)$ und betrachte $\kts_m$. Es gilt: $s_m \models \lnot P \U Q$ und $t_m \not{\models} \lnot P \U Q$. Nach Lemma \ref{lemma:ks_f_konstruktion} gilt aber: $s_m \models \phi \Leftrightarrow t_m \models \phi$. Widerspruch!
\end{beweis}

\begin{satz}[Hierarchiesatz]
	Für alle $m \geq 0$ gilt: $L(\X^m, \F) < L(\X^{m+1}, \F)$.
	
	Es gilt also: $\underbrace{L(\X^0, \F)}_{L(\F)} < L(\X^1, \F) < L(\X^2, \F), \dots$
\end{satz}

\begin{beweis}
	Sei $m \geq 0$. Betrachte $\kts_m$:
	
	Nach Lemma \ref{lemma:ks_f_konstruktion} gilt $s_m \models \phi \Leftrightarrow t_m \models \phi, \forall \phi \in L(\X^m, \F)$.
	
	Die Formel $\phi =_{\text{def}} \underbrace{\X \dots \X}_{(m + 1) - \text{mal}} Q \in L(\X^{m+1}, \F)$ kann die Pfade von den Zuständen $s_m$ und $t_m$ aber unterscheiden:
	\[
		s_m \models \phi \text{ und } t_m \not{\models} \phi
	\].
\end{beweis}

\begin{defn}[Syntax von $\fol$]
	Sei $\vars$ eine abzählbare Menge von Individuenvariablen. Es werden Formeln der Logik erster Stufe linearer Ordnung (first order logic of linear order) gemäß der folgenden (abstrakten) Grammatik gebildet:
	\[
		\phi ::= p(x) \mid x_1 = x_2 \mid x_1 < x_2 \mid \lnot \phi \mid \phi_1 \land \phi_2 \mid \exists x \phi
	\]
	wobei $x_1, x_2 \in \vars$ und $p \in AP$. Die Menge der Formeln bezeichnen wir mit $\fol$.
\end{defn}

Intuitiv sagen wir: Variablen laufen über die Positionen in einem Pfad.

\begin{defn}[Semantik von $\fol$]
	Wir definieren die Semantik von $\fol$ induktiv über den Formelaufbau. Hierbei betrachten wir Pfade $\pi$ einer gegebenen Kripke-Struktur $\kts = (S, S_0, R, L)$.
	
	Abbildungen $\varepsilon: \vars \rightarrow \mathbb{N}_0$ heißen \emph{Umgebungen} (Zuordnungen von Positionen zu den Variablen).
	
	Für $\varepsilon: \vars \rightarrow \mathbb{N}_0, x \in \vars, n \in \mathbb{N}_0$ sei $\varepsilon[x \mapsto n]$ die folgende Umgebung:
	
	\[
		(\varepsilon[x \mapsto n])(y) = 
		\begin{cases}
			n, &\text{falls } x = y \\
			\varepsilon(y), &\text{sonst}
		\end{cases}
	\]
	
	\begin{tabular}[]{ll}
		$\pi \modelse p(x)$ & gdw. $p \in L(\pi(\varepsilon(x)))$ \\
		$\pi \modelse x_1 = x_2$ & gdw. $\varepsilon(x_1) = \varepsilon(x_2)$ \\
		$\pi \modelse x_1 < x_2$ & gdw. $\varepsilon(x_1) < \varepsilon(x_2)$ \\
		$\pi \modelse \lnot \phi$ & gdw. nicht $\pi \modelse \phi$ \\
		$\pi \modelse \phi_1 \land \phi_2$ & gdw. $\pi \modelse \phi_1$ und $\pi \modelse \phi_2$ \\
		$\pi \modelse \exists x: \phi$ & gdw. es gibt $n \in \mathbb{N}_0$ mit $\pi \models_{\varepsilon[x \mapsto n]} \phi$ \\
	\end{tabular}

	Sei $\phi$ eine $\fol$-Formel, in der nur die Variable $x$ frei vorkommt. Wir sagen dann, dass $\pi \phi(x)$ \emph{initial erfüllt} (d.h. ab Position $0$), in Zeichen $\pi \models_i \phi(x)$ gdw. $\pi \models_{\varepsilon_0} \varphi(x)$ für eine Umgebung $\varepsilon_0$ mit $\varepsilon_0(x) = 0$.
\end{defn}

\begin{satz}[$\lxu \leq \fol$]
	$\lxu \leq \fol$, das heißt zu jeder Formel $\phi \in \lxu$ gibt es eine Formel $\tilde{\phi}(x) \in \fol$, die höchstens $x$ als freie Variable hat, so dass für alle Pfade $\pi$ gilt:
	\[
		\underbrace{\pi \models \phi}_{\text{im } \lxu \text{-Sinne}} \iff  \underbrace{\pi \models_i \tilde{\phi}(x)}_{\text{im } \fol \text{-Sinne}}
	\]
\end{satz}
\begin{beweis}
	Wir definieren induktiv über den Aufbau von $\phi$ eine Übersetzungsfunktion $Tr: \lxu \times \vars \rightarrow \fol$, so dass für alle $\phi \in \lxu, x \in \vars$ gilt:
	\begin{equation}
		\tag{$\star$} \pi^n \models \phi \iff \pi \models_{\varepsilon[x \mapsto n]}Tr(\phi, x)
	\end{equation}
	für alle Pfade $\pi$ und Positionen $n \in \mathbb{N}_0$.
	
	$Tr$ ist definiert durch:
	\begin{itemize}
		\item $Tr(p,x) = p(x)$
		\item $Tr(\lnot \phi, x) = \lnot Tr(\phi, x)$
		\item $Tr(\phi_1 \land \phi_2, x) = Tr(\phi_1, x) \land Tr(\phi_2, x)$
		\item $Tr(\X\phi, x) = \exists y (\underbrace{x < y \land \lnot(\exists z: x < z \land z < y)}_{y = x + 1} \land Tr(\phi, y))$
		\item $Tr(\phi_1 \U \phi_2, x) = \exists k: (\underbrace{(x < k \lor x = k)}_{x \leq k} \land Tr(\phi_2, k) \land \forall l: (\underbrace{((x < l \lor x = l) \land l < k)}_{x \leq l < k} \Rightarrow Tr(\phi_1, l)))$
	\end{itemize}

	Die Gültigkeit von $(\star)$ wird über strukturelle Induktion bewiesen.
	
	Wähle dann: $\tilde{\phi} = Tr(\phi,x)$.
\end{beweis}

\begin{satz}[Satz von Kamp (Ausdrucksvollständigkeit, expressive completeness)]
	\[\fol \leq \lxu, \]
	d.h. zu jeder Formel $\phi(x) \in \fol$ (mit höchstens $x$ als freien Variablen) gibt es eine Formel $\tilde{\phi} \in \lxu$, so dass für alle Pfade $\pi$:
	\[
		\underbrace{\pi \models_i \tilde{\phi}(x)}_{\text{im } \fol \text{-Sinne}} \iff \underbrace{\pi \models \phi}_{\text{im } \lxu \text{-Sinne}}
\]
	Der Beweis ist nicht trivial und erfordert mehr Vorarbeiten; Daher wird er an dieser Stelle nicht geführt.
\end{satz}

Mit der Aussage dieses Satzes können wir sehen, dass man alles, was durch Sprechen über einzelne Positionen, Positionsvergleiche und Gültigkeit atomarer Proposition an Positionen gesagt werden kann, in $\lxu$ spezifizieren kann (überraschenderweise!). Dies ist ein gutes Argument für $\lxu$ (also LTL). 

\begin{bem}
	Man kann sogar zeigen, dass sich jede Modalität, deren Semantik durch eine prädikatenlogische Formel erster Stufe über linearen Ordnungen definieren lässt, in in LTL als Abkürzung einführen lässt.
\end{bem}

\begin{bsp}[Modalität \enquote{twice during}]
	$\pi \models \phi \TD \psi$ gdw. $\exists l_1, l_2, l_1 < l_2 \land \pi^{l_1} \models \phi, \pi^{l_2} \models \phi$ und $\forall m: l_1 \leq m \leq l_2 \Rightarrow \pi^m \models \psi$
	
	Mittels den bekannten Formel ist dies: $\phi \TD \psi \equiv \F(\phi \land \psi \land \X (\psi \U (\phi \land \psi)))$
\end{bsp}

\subsection{Branching-Time Logik}

Sei $\kts = (S, S_0, R, L)$ eine (totale) Kripke-Struktur. Wir wollen nun zusätzlich Pfad Quantoren $\A$ (für alle Pfade) und $\E$ (es gibt einen Pfad...) betrachten.

\begin{defn}[Pfadformeln]
	\begin{itemize}
		\item $\pi \models \A\phi \Leftrightarrow_{\text{def}}$ für alle Pfade $\pi'$ (der unterliegenden Kripke-Struktur) mit $\pi(0) = \pi'(0)$ gilt $\pi' \models \phi$.
		\item $\pi \models \E\phi \Leftrightarrow_{\text{def}}$ es existiert ein Pfad $\pi'$ (der unterliegenden Kripke-Struktur) mit $\pi(0) = \pi'(0)$ gilt $\pi' \models \phi$.
	\end{itemize}
\end{defn}

\begin{bsp}
	\begin{enumerate}[a)]
		\item $\E \textit{rain}$ (Regen ist möglich)
		\mbox{}
		\begin{center}
			\begin{tikzpicture}
				\node[draw, circle, minimum height = 1.5mm](0) at (0,0){};
				\node[draw, circle, minimum height = 1.5mm](1) at (-2,-0.5){};
				\node[draw, circle, minimum height = 1.5mm](2) at (2,-0.5){};
				\node[draw, circle, minimum height = 1.5mm](31) at (-3,-1.5){};
				\node[draw, circle, minimum height = 1.5mm](32) at (-2,-1.5){};
				\node[draw, circle, minimum height = 1.5mm](33) at (-1,-1.5){};
				\node[draw, circle, minimum height = 1.5mm](41) at (3,-1.5){};
				\node[draw, circle, minimum height = 1.5mm](42) at (2,-1.5){};
				\node[draw, circle, minimum height = 1.5mm](43) at (1,-1.5){\scriptsize r};
				
				\path[->]
				(0) edge[] (1)
				(0) edge[] (2)
				(1) edge[] (31)
				(1) edge[] (32)
				(1) edge[] (33)
				(2) edge[] (41) 
				(2) edge[] (42) 
				(2) edge[] (43); 
			\end{tikzpicture}
		\end{center}
		\item $\A \textit{rain}$ (Regen ist unausweichlich)
		\mbox{}
		\begin{center}
			\begin{tikzpicture}
				\node[draw, circle, minimum height = 1.5mm](0) at (0,0){};
				\node[draw, circle, minimum height = 1.5mm](1) at (-2,-0.5){\scriptsize r};
				\node[draw, circle, minimum height = 1.5mm](2) at (2,-0.5){};
				\node[draw, circle, minimum height = 1.5mm](31) at (-3,-1.5){};
				\node[draw, circle, minimum height = 1.5mm](32) at (-2,-1.5){};
				\node[draw, circle, minimum height = 1.5mm](33) at (-1,-1.5){};
				\node[draw, circle, minimum height = 1.5mm](41) at (3,-1.5){};
				\node[draw, circle, minimum height = 1.5mm](43) at (1,-1.5){\scriptsize r};
				\node[draw, circle, minimum height = 1.5mm](51) at (4,-2.5){\scriptsize r};
				\node[draw, circle, minimum height = 1.5mm](53) at (2,-2.5){\scriptsize r};
				
				\path[->]
				(0) edge[] (1)
				(0) edge[] (2)
				(1) edge[] (31)
				(1) edge[] (32)
				(1) edge[] (33)
				(2) edge[] (41) 
				(41) edge[] (51) 
				(41) edge[] (53) 
				(2) edge[] (43); 				
			\end{tikzpicture}
		\end{center}
		\item $\A\G(M \Rightarrow \E\X K)$ (Ich kann immer nach dem Münzeinwurf Kaffee erhalten)
		\mbox{}
		\begin{center}
			\begin{tikzpicture}
				\node[]() at (-2.5, 0.5) {$\mathcal{K}_1$};
				\node[state, initial](0) at (0,0) {0};
				\node[state, label = {[align=left, right, xshift=3mm]M}](1) at (0,-2){1};
				\node[state, label = {[align=left, xshift=-2mm,left]K}](2) at (-2,-4){2};
				\node[state, label = {[align=left, xshift=2mm, right]T}](3) at (2,-4){3};
				
				\path[->]
				(0) edge [] (1) 		
				(1) edge []  (2) 		
				(1) edge [] (3) 		
				(2) edge [bend left=20] (0) 		
				(3) edge [bend right=20] (0);		
			\end{tikzpicture}
			\hspace{2cm}
			\begin{tikzpicture}					
				\node[]() at (-2.5, 0.5) {$\mathcal{K}_2$};
				\node[state, initial](0) at (0,0) {0};
				\node[state, label = {[align=left, right, xshift=-8mm]M}](m1) at (-2,-2){1};
				\node[state, label = {[align=left, right, xshift=3mm]M}](m2) at (2,-2){1};
				\node[state, label = {[align=left, xshift=-2mm,left]K}](2) at (-2,-4){2};
				\node[state, label = {[align=left, xshift=2mm, right]T}](3) at (2,-4){3};
				
				\path[->]
				(0) edge [] (m1) 		
				(0) edge [] (m2) 		
				(m1) edge [] (2) 		
				(m2) edge [] (3) 		
				(2) edge [bend right=20] (0) 		
				(3) edge [bend left=20] (0);				
			\end{tikzpicture}
		\end{center}
		\begin{itemize}
			\item $\kts_1$ erfüllt die Formel.
			\item $\kts_2$ erfüllt die Formel nicht.
		\end{itemize}
	\end{enumerate}
\end{bsp}

\begin{bsp}[Illustration]
	\todo{Bild einfügen}
\end{bsp}

Wir definieren \emph{CTL*} als LTL + $\E, \A$

$\phi ::= p \mid \phi_1 \land \phi_2 \mid \lnot \phi_1 \mid \X \phi_1 \mid \phi_1 \U \phi_2 \mid \E \phi_1 \mid \A \phi_1 [+ \F \phi \mid \G \phi \mid \phi_1 \W \phi_2 \mid \phi_1 \R \phi_2]$

\begin{bem}
	$\A$ und $\E$ sind duale Operatoren:
	\begin{enumerate}[i)]
		\item $\lnot \A \phi \equiv \E \lnot \phi$
		\item $\lnot \E \phi \equiv \A \lnot \phi$
	\end{enumerate}

	\begin{beweis}
		Dieser ist eigentlich offensichtlich, wir wollen ihn hier dennoch führen.
		
		\begin{enumerate}[i)]
			\item 
			\begin{align}
				&\pi \models \lnot \A \phi\\
				\iff & \text{nicht für alle Pfade} \pi' \text{ mit  } \pi(0) = \pi'(0) \text{ gilt } \pi' \models \phi & [\text{Def} \models] \\
				\iff & \text{es existiert ein Pfad} \pi' \text{ mit } \pi(0) = \pi'(0) \text{ und } \underbrace{\text{ nicht } \pi' \models \phi}_{\pi' \models \lnot \phi} \\
				\iff & \pi \models \E \lnot \phi & [\text{Def} \models] \\
			\end{align}
			\item $\lnot \E \phi \equiv \lnot \E \lnot \lnot \phi \underset{(i)}{\models} \lnot \lnot \A \lnot \phi \models \A \lnot \phi$
		\end{enumerate}
	\end{beweis}
\end{bem}

\begin{bem}
	$\A \phi$ und $\E \phi$ sind \emph{Zustandsformeln}: Eine Formel $\psi$ heißt Zustandsformel, gdw. $\forall \text{ Pfade } \pi, \pi': \pi(0) = \pi'(0) \Rightarrow (\pi \models \psi \Leftrightarrow \pi' \models \psi)$
\end{bem}

Eine Kombination aus Pfadquantor und LTL-Modalität nennen wir eine CTL-Modalität.

Liberale Definition von CTL:
\begin{itemize}
	\item \enquote{Zustandsformeln}: $\phi ::= p \mid \phi_1 \land \phi_1 \mid \lnot \phi_1 \mid \A \psi \mid \E \psi$
	\item \enquote{Pfadformeln}: $\psi ::= \X \phi \mid \F \phi \mid \G \phi \mid \phi_1 \U \phi_2 \mid \phi_1 \W \phi_2 \mid \phi_1 \R \phi_2$
\end{itemize}

Alle diese Formeln kann man in \enquote{Basis-CTL} übersetzen:
\[
	\phi ::= p \mid \phi_1 \land \phi_2 \mid \lnot \phi \mid \E \X \phi \mid \A(\phi_1 \U \phi_2) \mid \E(\phi_1 \U \phi_2)
\]

z.B. gilt:
\begin{itemize}
	\item $\A \X \phi \equiv \lnot \lnot \A \X \phi \equiv \lnot \E \lnot \X \phi \equiv \lnot \E \X \lnot \phi$
	\item $\A \F \phi \equiv \A (\texttt{true} \U \phi)$
	\item $\A \G \phi \equiv \A(\lnot \F \lnot \phi) \equiv \A(\lnot (\texttt{true} \U \lnot \phi)) \equiv \lnot \E(\texttt{true} \U \lnot \phi)$
\end{itemize}

Beachte: Zur Herleitung rechnen wir in CTL* und verwenden die Dualitätsgesetze.

\begin{bem}
	In Hinblick auf Gültigkeit für Kripke-Strukturen gilt:
	\[
		\text{CTL} < \text{CTL*}, \text{LTL} < \text{CTL*}, \text{LTL} \nleq \text{CTL}, \text{CTL} \nleq \text{LTL}.
	\]
	
	Zum Beweis: Die Aussagen über CTL* folgen aus CTL $\subseteq$ CTL* und LTL $\subseteq$ CTL* und den restlichen Aussagen.
	
	CTL $\nleq$ LTL folgt aus dem Münzautomatenbeispiel.
	
	Der Beweis von LTL $\nleq$ CTL wird hier nicht komplett geführt. Wir behaupten aber:
	\begin{itemize}
		\item Die LTL-Formel $\F\G p$ kann man in CTL nicht ausdrücken
		\item Die CTL-Formel $\A\F\G\A p$ kann nicht in LTL ausgedrückt werden.
	\end{itemize}

	Beachte:
	
	\begin{center}
		\begin{tikzpicture}
			\node[]() at(0,0) {$\kts$:};
			\node[state, initial, label={[above]$P$}](1) at (3,0) {};
			\node[state, label={[above]$\lnot P$}](2) at (5,0) {};
			\node[state, label={[above]$P$}](3) at (7,0) {};
			
			\path[->]
			(1) edge[] (2)
			(2) edge[] (3)
			(3) edge[loop below] (3)
			(1) edge[loop below] (1);
		\end{tikzpicture}
	\end{center}
	
	Dann gilt $\kts \models \F\G p$, aber $\kts \not{\models} \A\F\A\G p \qed$
	
\end{bem}

\cleardoubleoddemptypage