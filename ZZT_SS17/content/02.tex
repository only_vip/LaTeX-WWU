%!TEX root = ../ZZT_SS17.tex
\section{Kongruenzen}
\label{sec:2}
	\begin{definition}[Kongruenz]
		\label{def:2.1}
		Sei $a,b,m \in \ZZ$. \marginnote{[1] \\ 11.5.}
		Wenn $m \mid (a-b)$ gilt, so schreiben wir
		\[
			a \kon b  \modu{m} \qquad \text{bzw.} \qquad a \kon b (m) \marginnote{lies \enquote{$a$ kongruent $b$ modulo $m$}}
		\]
		Umformulierung: $b$ entsteht aus $a$ durch Addition oder Subtraktion eines Vielfachen von $m$. \index{Kongruenz}
	\end{definition}

	\textbf{Beispiele:} \begin{itemize}
		\item $5 \kon 7 \modu{2}$
		\item $5 \not\kon 7 \modu{3}$
		\item $a \kon 0 \modu{2} \Leftrightarrow a$ gerade
		\item $a \kon b \modu{0} \Leftrightarrow a = b$
	\end{itemize}
	\begin{lemma}
	\label{lemma:2.2}
		Sei $a,b,c,m \in \ZZ$. \marginnote{[2]}
		Dann gilt:	
	\begin{enumerate}[(i)]
		\item $a \kon 0 \modu{m} \Leftrightarrow a \in m\ZZ \Leftrightarrow m \mid a$
		\item $a \kon a \modu{m}$ gilt immer.
		\item $a \kon b \modu{m} \Leftrightarrow b \kon a \modu{m}$.
		\item $a \kon b \modu{m}$ und $b \kon c \modu{m} \Rightarrow a \kon c \modu {m}$.
	\end{enumerate}
	\end{lemma}

	\begin{beweis}
		\textbf{(i)} und \textbf{(ii)} sind klar.
		\begin{enumerate}[(i)] \setcounter{enumi}{2}
			\item $a \kon b \modu{m} \Leftrightarrow m \mid a -b \Leftrightarrow m \mid b-a \Leftrightarrow b \kon a \modu{m}$.
			\item $a-b = sm$ und $b-c = tm \Rightarrow a-c = (sm + b) + (tm - b) = (s+t) \cdot m$.
		\end{enumerate}
	\end{beweis}

	\textbf{Folgerung:} Ist $m \in \ZZ$ fest gewählt, so ist Kongruenz modulo $m$ eine Äquivalenzrelation auf $\ZZ$ (reflexiv, symmetrisch und transitiv).
	
	Der Zusammenhang zum Teilen mit Rest ist folgender:
	Sei $m > 0$.
	Sei $a,a' \in \ZZ$.
	Teile durch $m$ mit Rest:
	\[
		\begin{array}{rl}
			a = ms + r & 0 \leq r < m \\
			a' = ms' + r' & 0 \leq r' < m
		\end{array}
	\]
	Dann gilt $a \kon a' \modu{m} \Leftrightarrow r = r'$.
	
	\begin{satz}
		\label{satz:2.3}
		Sei $m \in \ZZ$. \marginnote{[3]}
		Dann ist Kongruenz modulo $m$ verträglich mit Addition, Subtraktion und Multiplikation.
		Genauer:
		Sind $a,a',b,b' \in \ZZ$ mit $a \kon a' \modu{m}, b \kon b' \modu{m}$, so gilt
		\begin{align*}
			a + b &\kon a' + b' \modu{m} \\
			a-b &\kon a'-b' \modu{m} \\
			a \cdot b &\kon a' \cdot b' \modu{m}
		\end{align*}
	\end{satz}

	\begin{beweis}
		Schreibe $a' = a + sm, b' = b + tm$.
		Es folgt
		\begin{align*}
			a' + b'&= a+b + (s+t) \cdot m \\
			a'-b' &= a-b+(s-t) \cdot m \\
			a' \cdot b' &= ab + (bs+at+st) \cdot m
		\end{align*}
	\end{beweis}

	\textbf{Vorsicht:} Das Kürzen ist bei Kongruenzen im Allgemeinen nicht erlaubt!
	Zum Beispiel ist $2 \cdot 3 \kon 0 \modu{6}$, aber $2 \not\kon 0 \modu{6}$ und $3 \not\kon 0 \modu{6}$.
	
	\begin{anw}[Teilbarkeitsregeln]
		\label{anw:2.4}
		Eine Zahl ist durch $3$ (bzw. $9$) teilbar genau dann, wenn ihre Quersumme durch $3$ (bzw. $9$) teilbar ist. \marginnote{[4]}
		
		Genauer: Sei $n \in \NN$ mit Dezimaldarstellung \index{Quersumme} \index{Teilbarkeitsregel}
		\[
			n = \sum_{j=0}^{k} 10^j \cdot a_j, 0 \leq a_j \leq 9,
		\]
		zum Beispiel $1044 = 4 \cdot 10^0 + 4 \cdot 10^1 + 0 \cdot 10^2 + 1 \cdot 10^3$.
		Die Quersumme ist also $a_0+a_1+\dots+a_k$.
		Es gilt $10 \kon 1 \modu{3}$ sowie $10 \kon 1 \modu{9}$, also für alle $j \geq 0$
		\begin{align*}
			10^j &\kon 1 \modu{3} \\
			10^j &\kon 1 \modu{9}.
		\end{align*}
		Damit erhalten wir:
		\begin{align*}
			3 \mid n &\Leftrightarrow n \kon 0 \modu{3} \\
			&\Leftrightarrow \sum_{j=0}^{k} 10^j \cdot a_j \kon 0 \modu{3} \\
			&\Leftrightarrow \sum_{j=0}^{k} a_j \kon 0 \modu{3} \\
			&\Leftrightarrow 3 \mid a_0 + a_1 + \dots + a_k
		\end{align*}
		Analog $9 \mid n \Leftrightarrow 9 \mid a_0 + a_1 + \dots + a_k$.
		
		Ganz ähnlich lässt sich beweisen, dass $n$ gerade ist, wenn $a_0$ gerade ist:
		\begin{align*}
			n \text{ gerade } &\Leftrightarrow 2 \mid n \Leftrightarrow n \kon 0 \modu{2} \\
			&\Leftrightarrow a_0 + 10 \cdot a_1 + \dots + 10^k \cdot a_k \kon 0 \modu{2} \\
			&\Leftrightarrow a_0 \kon 0 \modu{2}.
		\end{align*}
		Ebenso: $n$ ist durch $5$ teilbar genau dann, wenn $a_0$ durch $5$ teilbar ist.
		
		Teilbarkeit durch $11$: Es gilt $10 \kon -1 \modu{11}$ und damit
		\begin{align*}
			11 \mid n &\Leftrightarrow n \kon 0 \modu{11} \\
			&\Leftrightarrow \underbrace{a_0 - a_1 + a_2 - a_3 + a_4 \dots}_{\text{alternierende Quersumme}} \kon 0 \modu{11}
		\end{align*}
	\end{anw}

	\begin{satz}
		\label{satz:2.5}
		Sei $a,b,c,m \in \ZZ$. \marginnote{[5]}
		Wenn $\ggT(c,m) = 1$ und $ac \kon bc \modu{m}$ gilt, so folgt $a \kon b \modu{m}$.
	\end{satz}

	\begin{beweis}
		Wenn $ac \kon bc \modu{m}$, dann gilt $m \mid (a-b)\cdot c$.
		Nach Lemma~\ref{lemma:1.20.1} folgt $m \mid a-b$, da $\ggT(m,c) = 1$.
		Also ist $a \kon b \modu{m}$.
	\end{beweis}

	Zum Beispiel gilt
	\begin{align*}
		4 \cdot x &\kon 1 \modu{15} \\
		\Leftrightarrow \quad 4 \cdot x &\kon 16 \modu{15} \\
		\Leftrightarrow \quad x &\kon 4 \modu{15},
	\end{align*}
	weil $\ggT(4,15) = 1$.
	
	\begin{anw}
		Wir lösen damit die lineare Diophantische Gleichung $9x + 16y = 35$: \marginnote{[6]}
		\begin{align*}
			9x + 16y = 35 &\Leftrightarrow 16y \kon 35 \modu{9} \\
				&\Leftrightarrow 7y \kon 35 \modu{9} \\
				&\Leftrightarrow y \kon 5  \modu{9} \\
				&\Leftrightarrow y = 5+9t,
		\end{align*}
		da $\ggT(9,7) = 1$.
		
		Insgesamt folgt
		\begin{align*}
				9x + 16 (5+9t) &= 35 \\
				\Leftrightarrow \quad 9x + 144t + 80 &= 35 \\
				\Leftrightarrow \quad 9x + 144t + 45 &= 0 \\
				\Leftrightarrow \quad x + 16t + 5 &= 0 \\
				\Leftrightarrow \quad x = -16t - 5)
		\end{align*}
		Lösungsmenge ist also $L = \{(-16t-5,9t+5) : t \in \ZZ\}$.
	\end{anw}

	\begin{definition}[Lineare Kongruenz]
		\label{def:2.7}
		Sei $a,b,m \in \ZZ$. \marginnote{[7] \\ 15.5.}
		Eine Gleichung der Form 
		\[
			ax \kon b \modu{m}
		\]
		heißt \Index{lineare Kongruenz}.
		Gesucht sind Lösungen $x \in \ZZ$.
	\end{definition}

	Offensichtlich äquivalent dazu ist das Problem, alle $x \in \ZZ$ zu bestimmen, sodass
	\[
		ax+ym = b
	\]
	für ein $y \in \ZZ$ gilt.
	Das ist eine lineare Diophantische Gleichung, also erhalten wir folgenden Satz:
	
	\begin{satz}
		\label{satz:2.7}
		Seien $a,b,m \in \ZZ$ mit $m > 0$.
		Die lineare Kongruenz $ax \kon b \modu{m}$ ist genau dann lösbar, wenn gilt: $\ggT(a,m) \mid b$.
	\end{satz}

	\begin{beweis}
		Folgt mit der vorigen Umformulierung aus Satz~\ref{satz:1.19}.
	\end{beweis}
		
	\begin{bemerkung}
		\label{bem:2.8}
		Falls $d = \ggT(a,m) \mid b$ gilt, so folgt aus Satz~\ref{satz:1.20.3}, wie die Lösungsmenge aussieht: \marginnote{[8]}
		Ist $x_0 \in \ZZ$ mit $ax_0 \kon b \modu{m}$, so ist jede weitere Lösung $x$ der linearen Kongruenz $ax \kon b \modu{m}$ von der Form $x = x_0 + tm'$, wobei $dm' = m$ und $t \in \ZZ$.
	\end{bemerkung}

	Satz~\ref{satz:2.5} lässt sich verbessern, das ist zum Rechnen hilfreich:
	
	\begin{satz}
		\label{satz:2.9}
		Sei $a,b,c,m \in \ZZ$ mit $m > 0$, sei $d = \ggT(c,m)$ und $dm' = m$. \marginnote{[9] \\ vgl. auch Aufgabe~\ref{aufg:5.2}}
		Dann sind äquivalent:
		\begin{enumerate}[(i)]
			\item $ac \kon bc \modu{m}$
			\item $a \kon b \modu{m'}$
		\end{enumerate}
	\end{satz}

	\begin{beweis}
		Angenommen, $m = m'd \mid c(a-b)$, etwa $km = c(a-b)$.
		Schreibe $c = dc'$, so folgt:
		\begin{align*}
			m'dk &= c'd(a-b) \\
			\xRightarrow{d \neq 0} \quad m'k &= c'(a-b) \\
			\xRightarrow{\ggT(m',c')=1} \quad m' &\mid a-b
		\end{align*}
		
		Angenommen, $m' \mid a-b$, etwa $m'l = a-b$.
		Dann folgt $\underbrace{lcm'}_{lmc'} = c(a-b)$, also $m \mid c(a-b)$.
	\end{beweis}

	\begin{beispiel}
		Wir betrachten die lineare Kongruenz $6x \kon 15 \modu{33}$.
		\begin{align*}
			6x &\kon 15 \modu{33} \\
			\Leftrightarrow \quad 2x &\kon 5 \modu{11} \\
			\Leftrightarrow \quad 2x &\kon 16 \modu{11} \\
			\xLeftrightarrow{\ggT(2,11)=1} \quad x &\kon 8 \modu{11} \\
			\Leftrightarrow \quad x \in L &= \{11t+8 : t \in \ZZ\}
		\end{align*}
	\end{beispiel}

	\begin{ausblick}
		\label{aus:2.10}
		Für $a,m \in \ZZ$ definieren wir \textbf{Kongruenzklassen modulo $m$}: \marginnote{[10]}
		\begin{align*}
			[a]_m &= \{a' \in \ZZ : a \kon a' \modu{m}\} \\
			&= \{a' \in \ZZ : \text{ es gibt } t \in \ZZ \text{ mit } a' = mt + a\} = \{a + mt : t \in \ZZ\}
		\end{align*}
		Es gilt $[a]_m = [b]_m \Leftrightarrow b \in [a]_m \Leftrightarrow m \mid b-a$.
		
		Für $a,a',b,b' \in \ZZ$ mit $a \kon a' \modu{m}, b \kon b' \modu{m}$ folgt mit Satz~\ref{satz:2.3}, dass $[ab]_m = [a'b']_m$ und $[a+b]_m = [a'+b']_m$.
		Wir können deswegen Verknüpfungen $+$ und $\cdot$ definieren durch
		\begin{align*}
			[a]_m + [b]_m &= [a+b]_m \\
			[a]_m \cdot [b]_m &= [ab]_m
		\end{align*}
		Das führt es auf Gruppen und Ringe.
	\end{ausblick}
	
\cleardoubleoddemptypage