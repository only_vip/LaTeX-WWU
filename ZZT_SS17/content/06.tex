%!TEX root = ../ZZT_SS17.tex
\section{Die Transzendenz von $e$ und $\pi$}
\label{sec:6}

\begin{definition}[algebraisch]
	\label{def:6.1}
	Eine komplexe Zahl $z \in \CC$ heißt \Index{algebraisch}, wenn es ein Polynom $f \in \QQ[T]$ gibt mit $\deg(f) \geq 1$ und $f(z) = 0$ gilt. \marginnote{[1]}
\end{definition}

\begin{beispiel}
	\label{bsp:6.1}
	\begin{itemize}
		\item Jede rationale Zahl $q \in \QQ$ ist algebraisch, denn $q$ ist Nullstelle von $f = T-q \in \QQ[T]$.
		\item $\sqrt{2}$ ist nicht rational, aber algebraisch, denn $\sqrt{2}$ ist Nullstelle von $g = T^2 - 2 \in \QQ[T]$.
		
		Wäre $\sqrt{2} = \frac{a}{b}$ mit $a,b \in \ZZ, b \neq 0$, so setze $d = \ggT(a,b), a = a'd, b = b'd$.
		Dann ist $\sqrt{2} = \frac{a'}{b'}$ und $\ggT(a',b') = 1$.
		Es folgt $(a')^2 = 2 \cdot (b')^2$, also $2 \mid (a')^2$.
		Nach Lemma~\ref{lemma:1.13} gilt dann $2 \mid a'$.
		Schreibe $a' = 2^l \cdot \wt{a}$ mit $\wt{a}$ ungerade, $l \geq 1$.
		Da $\ggT(a',b') = 1$ ist $b'$ ungerade, aber
		$2^{2l} \cdot \wt{a}^2 = 2 \cdot b' = b' = 2^{2l-1} \cdot \wt{a}^2$. \lightning
	\end{itemize}
\end{beispiel}

\begin{definition}[transzendent]
	\label{def:6.2}
	Sei $A \subseteq \CC$ die Menge aller algebraischen Zahlen. \marginnote{[2]}
	Eine komplexe (oder reelle) Zahl $z \in \CC$ heißt \Index{transzendent}, falls $z$ nicht algebraisch ist, also $z \in \CC \setminus A$.
\end{definition}

\textbf{Heuristische Überlegung:} $\QQ$ ist abzählbar, jedes Polynom $f \in \QQ[T]$ hat endlich viele Koeffizienten.
Demnach ist auch $\QQ[T]$ abzählbar.
Da jedes Polynom $f \in \QQ[T]$ mit $f \neq 0$ höchstens $\deg(f)$ verschiedene Nullstellen hat, ist also auch $A$ abzählbar.
Hingegen sind $\CC$ und $\RR$ nicht abzählbar, also ist \enquote{fast jede} reelle oder komplexe Zahl transzendent.

\begin{erinner}[Partielle Integration]
	\label{erinner:6.3}
	\[
		\int_u^v (f \cdot g)' = \benb{f \cdot g}_u^v = \int_u^v f \cdot g' + \int_u^v f' \cdot g
	\]
	Falls $f(u) = f(v) = 0$, gilt also \marginnote{[3]}
	\[
		\int_u^v f \cdot g' = \int_u^v f' \cdot g
	\]
\end{erinner}

\begin{thm}
	\label{thm:6.4}
	Die Zahl $\pi$ ist irrational. \marginnote{[4]}
\end{thm}

\begin{beweis}
	Sei $\alpha \in \RR$ fest.
	Wir setzen
	\[
		I_n = \int_{-1}^{1} (1-x^2)^n \cos(\alpha x)dx.
	\]
	Mit Hilfe von \ref{erinner:6.3} erhält man:
	\begin{align*}
		I_0 &= \frac{2}{\alpha} \sin(\alpha) \\
		I_1 &= \frac{4}{\alpha^3} \sin(\alpha) - \frac{4}{\alpha^2} \cos(\alpha) \\
		I_n &= \frac{1}{\alpha^2} 2n(2n-1) I_{n-1} - \frac{1}{\alpha^2} 4n(n-1) I_{n-2} \qquad \text{für } n \geq 2
	\end{align*}
	Mit Induktion folgt daraus
	\[
		I_n = \frac{n!}{\alpha^{2n+1}} (f_n(\alpha) \cdot \sin(\alpha) + g_n(\alpha) \cos(\alpha))
	\]
	mit Polynomen $f_n, g_n \in \ZZ[T]$ und $\deg(f_n), \deg(g_n) \leq 2n+1$.
	
	Angenommen, $\pi \in \QQ$.
	Dann gibt es $a,b \in \ZZ, a,b > 0$ mit $\frac{\pi}{2} = \frac{a}{b}$.
	Es folgt mit $\alpha = \frac{\pi}{2}$ für jedes $n \geq 0$
	\begin{align*}
		\frac{1}{n!} \cdot \frac{a^{2n+1}}{b^{2n+1}} I_n &= f_n \enb{\frac{a}{b}} \underbrace{\sin\enb{\frac{\pi}{2}}}_{=1} \\
		\Leftrightarrow \quad \frac{1}{n'} a^{2n+1} I_n &= f_n \enb{\frac{a}{b}} \cdot b^{2n+1} \in \ZZ.
	\end{align*}
	Es gilt weiter $\abs{I_n} \leq 4$, also $f_n \enb{\frac{a}{b}} b^{2n+1}$ wegen $\lim\limits_{n \rightarrow \infty} \frac{a^{2n+1}}{n!} = 0$ für $n \gg 0$.
	Andererseits ist $\abs{I_n} > 0$, da der Integrand im Bereich $[-1,1]$ fast überall positiv ist. \lightning
\end{beweis}

\begin{erinner}[Exponentialfunktion]
	\label{erinner:6.5}
	Die \Index{Exponentialfunktion} ist für $z \in \CC$ gegeben durch \marginnote{[5]}
	\[
		\exp(z) = \sum_{k=0}^{\infty} \frac{z^k}{k!}.
	\]
	Es gilt $\exp(0) = 1$, $\exp(u+v) = \exp(u) \cdot \exp(v)$ und $\exp' = \exp$.
	Die Eulersche Zahl ist $e = \exp(1) = 2.71828\dots$
\end{erinner}

\begin{lemma}
	\label{lemma:6.6}
	Sei $h \colon \RR \rightarrow \RR$ unendlich oft differenzierbar. \marginnote{[6] \\ 13.7.}
	Sei $l \geq 0$ und sei $f(x) = (x-\lambda)^l h(x)$.
	Dann gilt
	\[
		f^{(k)}(\lambda) = \begin{cases}
			0, & 0 \leq k \leq l \\
			\frac{k!}{(k-l)!} h^{(k-l)}(\lambda), & k \geq l.
		\end{cases}
	\]
\end{lemma}

\begin{beweis}
	Mit Taylor-Reihen um $\lambda$:
	\[
		Th = \sum_{k=0}^{\infty} \frac{1}{k!} h^{(k)} (x-\lambda)^k, \quad Tf = \sum_{j=0}^{\infty} \frac{1}{j!} f^{(j)}(\lambda)(x-\lambda)^j = \sum_{k=0}^{\infty} \frac{1}{k!} h^{(k)} (x-\lambda)^{k+l}.
	\]
	Durch Koeffizientenvergleich folgt die Behauptung.
\end{beweis}

\begin{thm}[Hermite, 1879]
	\label{thm:6.7}
	Die Eulersche Zahl $e = \exp(1) = \sum_{k=0}^{\infty} \frac{1}{k!} \approx 2.71828\dots$ ist transzendent. \marginnote{[7]}
\end{thm}

\begin{beweis}
	Angenommen, $e$ wäre algebraisch.
	Dann gibt es $a_0,\dots,a_m \in \QQ$ mit $a_m \neq 0$ und $m \geq 1$ und $0 = a_me^m + a_{m-1}e^{m-1} + \dots + a_0$.
	Ohne Einschränkung sei $a_0,\dots,a_m \in \ZZ$ (sonst mit Hauptnenner multiplizieren) und $a_0 \neq 0$ (sonst durch $e$ teilen).
	
	Sei $p \in \PP$ mit $p > m, \abs{a_0}$.
	Dann ist $p \nmid m$ und $p \nmid a_0$.
	Setze
	\[
		f = f_p = \frac{1}{(p-1)!} T^{p-1}(T-1)^p(T-2)^p \cdots (T-m)^p.
	\]
	Es folgt für $0 \leq x \leq m$, dass $\abs{f_p(x)} \leq \frac{1}{(p-1)!} m^{p-1} m^{mp} \leq \frac{1}{(p-1)!} (m^{m+1})^p$, also konvergiert die Funktionenfolge $(f_p)_{p \in \PP}$ auf $[0,m]$ gleichmäßig gegen Null.
	Sei 
	\[
		F(x) = \sum_{k=0}^{\infty} f^{(k)}(x) = f(x) + f'(x) + f''(x) + \dots + f^{(mp+p-1)}(x) + 0.
	\]
	Es gilt
	\[
		\frac{d}{dx} (\exp(-x)F(x)) = \exp(-x)F(x) + \exp(-x)F'(x) = \exp(-x)(F'(x)-F(x)) = -\exp(-x) f(x),
	\]
	also
	\[
		\int_{0}^{j} \exp(-x)f(x) dx = \exp(-0) F(0) - \exp(-j)F(j)
	\]
	und damit
	\begin{align*}
		&\exp(j) \int_{0}^{j} \exp(-x) f(x)dx = e^jF(0) - F(j) \\
		\Rightarrow \quad &\sum_{j=0}^{m} a_j e^j \int_{0}^{j} \exp(-x)f(x)dx = \sum_{j=0}^{m} a_je^jF(0) - \sum_{j=0}^{m} a_jF(j).
	\end{align*}
	Mit Lemma~\ref{lemma:6.6} folgt für $j = 1, 2,\dots, m$, dass
	\[
		f^{(k)}(j) = \begin{cases}
			0, &\text{falls } 0 \leq < p \\
			\lambda p, \lambda \in \ZZ &\text{falls }k \geq p
		\end{cases} \qquad \qquad \qquad
		f^{(k)}(0) = \begin{cases}
			0, &\text{falls } 0 \leq k < p-1 \\
			(-1)(-2)(-3) \cdots (-m), &\text{falls } k = p-1 \\
			\lambda p, \lambda \in \ZZ &\text{falls }k \geq p
		\end{cases}
	\]
	Also ist $F(j) \in \ZZ$ für $j = 0,1,\dots,m$ und $F(j) \kon 0 \modu{p}$ für $j = 1,2,\dots,m$ sowie $F(0) \kon (-1)^m m! \modu{p}$.
	\[
		\Rightarrow \sum_{j=0}^m a_j F(g) \in \ZZ \text{ und } \sum_{j=0}^{m} a_j F(j) \kon a_0(-1)^m m! \modu{p}.
	\]
	Für $p > m, \abs{a_0}$ und $a_0 \neq 0$ folgt
	$\sum_{j=0}^{m} a_jF(j) \in \ZZ \setminus \setzero$.
	
	Andererseits gilt
	\[
		\lim\limits_{p \in \PP} \sum_{j=0}^{m} a_j e^j \int_{0}^{j} \exp(-x)f_p(x)dx = 0,
	\]
	da die $f_p$ gleichmäßig gegen Null konvergieren. \lightning
\end{beweis}

\begin{definition}[symmetrisches Polynom]
	\label{def:6.9}
	Ein Polynom $f(u_1,\dots,u_n)$ in $n$ Variablen $u_1,\dots,u_n$ heißt \textbf{symmetrisch}, wenn für jede Permutation $\varrho$ der Indexmenge $\{1,\dots,n\}$ gilt: $f(u_1,\dots,u_n) = f(u_{\rho(1)},\dots,u_{\rho(n)})$. \index{Polynom!symmetrisch} \marginnote{[9]}
\end{definition}

\begin{beispiel}
	\label{bsp:6.9}
	\begin{description}
		\item[$n=1$:] Jedes Polynom in einer Variablen ist symmetrisch.
		\item[$n=2$:] $u_1^3 + u_2^3$ ist symmetrisch, $u_1u_2^2 + u_2^2 + u_2^2 \neq u_2u_1^2 + u_1^2$ sind nicht symmetrisch.  
	\end{description}
	Betrachte
	\[
		(T-u_1) \cdots (T-u_n) = T^n - \sigma_1 T^{n-1} + \sigma_2 T^{n-2} + \dots + (-1)^n \sigma_n
	\]
	mit
	\begin{align*}
		\sigma_1 &= u_1 + u_2 + \dots + u_n \\
		\sigma_2 &= u_1u_2 + u_2u_3 + \dots = \prod_{i<j} u_iu_j \\
		&\vdots \\
		\sigma_n &= u_1u_2 \cdots u_n.
	\end{align*}
	Diese $n$ Polynome heißen \textbf{elementare symmetrische Polynome}.
	Ist $g(u_1,\dots,u_n)$ ein beliebiges Polynom, so ist $g(\sigma_1,\dots,\sigma_n)$ symmetrisch.
\end{beispiel}

\begin{lemma}[Newton]
	\label{lemma:6.10}
	Sei $R$ ein kommutativer Ring (etwa $R = \ZZ$), sei $f(u_1,\dots,u_n)$ ein symmetrisches Polynom mit Koeffizienten in $R$. \marginnote{[10] \\ 17.7}
	Dann gibt es ein Polynom $h(u_1,\dots,u_n)$ mit Koeffizienten in $R$ so, dass $h(\sigma_1,\dots,\sigma_n) = f(u_1,\dots,u_n)$.
\end{lemma}

\begin{beweis}
	Wir ordnen die Monome $u_1^{l_1} \cdots u_n^{l_n}$ in $f$ so, dass stets $u_1^{l_1} \cdots u_n^{l_n}$ links von $u_1^{m_1} \cdots u_n^{m_n}$ steht genau dann, wenn $l_1 = m_1, l_2 = m_2, \dots, l_j > m_j$.
	Da $f$ symmetrisch ist, gilt für den Term ganz links $l_1 \geq l_2 \geq \dots \geq l_n$, $f = au_1^{l_1} u_2^{l_2} \cdots u_n^{l_n} + \dots$.
	Im Polynom
	\[
		p_1 = a\sigma_1^{l_1-l_2} \sigma_2^{l_2 - l_3} \cdots \sigma_n^{l_n}
	\]
	steht am Anfang der gleiche Term.
	Betrachte $f_1 = f - p_1$ und schaffe davon den ersten Term weg, usw.
	Das Verfahren terminiert nach endlich vielen Schritten, da $\deg(p_1) \leq \deg(f)$.
	Also ist $\deg(f_1) \leq \deg(f)$.
\end{beweis}

\begin{beispiel}
	\label{bsp:6.10}
	$n = 2, f = u_1^2 + u_2^2, \sigma_1 = u_1 + u_2, \sigma_2 = u_1 \cdot u_2$.
	\[
		\Rightarrow p_1 = \sigma_1^2 \cot \sigma_2^0 = u_1^2 + 2u_1u_2 + u_2^2
	\]
	$f_1 = -2u_1u_2, p_2 = -2\sigma_1^{1-1} \sigma_2^1 = -2u_1u_2, f_2 = f_1-p_2 = 0$
	\[
		\Rightarrow f = p_1 p_2 = \sigma_1^2 - 2\sigma_2.
	\]
\end{beispiel}

\begin{erinner}[Eulersche Formel]
	\label{erinnerung:6.11}
	Für $i = \sqrt{-1} \in \CC$ und $t \in \RR$ gilt \marginnote{[11]}
	\[
		\exp(it) = \cos(t) + i \cdot \sin(t).
	\]
	Insbesondere ist $\exp(i\pi) = -1$.
\end{erinner}

\begin{thm}[Lindemann, 1882]
	\label{thm:6.12}
	Die Zahlen $\pi$ und $i\pi$ sind transzendent. \marginnote{[12]}
\end{thm}

\begin{beweis}
	Angenommen, $\pi$ ist algebraisch, also
	\[
		a_m \pi^m + \dots + a_0 = 0
	\]
	für $a_j \in \QQ, m \geq 1, a_m \neq 1$.
	Dann ist $i\pi$ Nullstelle des Polynoms
	\[
		(a_m(iT)^m + \dots + a_0)(a_m(-iT)^m + \dots + a_0) \in \QQ[T],
	\]
	also ist auch $i\pi$ algebraisch.
	Folglich reicht es zu zeigen, dass $i\pi$ nicht algebraisch ist.
	Wir nehmen an, $i \pi$ wäre algebraisch und führen das zu einem Widerspruch.
	
	Angenommen, es gibt ein Polynom $h_1 \in \QQ[T]$ mit $\deg(h_1) \geq 1$ und $h_1(i\pi) = 0$.
	Da $\CC$ algebraisch abgeschlossen ist, gibt es $\lambda_1,\dots,\lambda_n \in \CC$ mit
	\begin{align*}
		h_1 &= (T - \lambda_1)(T-\lambda_2) \cdots (T-\lambda_n) & \text{o.E. } \lambda_1 = i\pi  \\
			&= T^n + a_{n-1}T^{n-1} + \dots + 0 & a_j \in \QQ \\
			&= T^n - \sigma_1(\lambda_1,\dots,\lambda_n) T^{n-1} + \dots + (-1)^n \sigma_n(\lambda_1,\dots,\lambda_n),
	\end{align*}
	das heißt die elementar symmetrischen Polynome in den $\lambda_i$ sind rational.
	
	Weiter gilt $\exp(i\pi) = -1$, also
	\begin{equation}
		0 = (\exp(\lambda_1) + 1) \cdots (\exp(\lambda_n) + 1) = \exp(\lambda_1 + \dots + \lambda_n) + \dots + \sum_{j < k} \exp(\lambda_j + \lambda_k) + \sum_{j} \exp(\lambda_j) + 1 \label{eq:6.12}
	\end{equation}
	Betrachte nun die Polynome
	\begin{align*}
		h_1 &= \prod_{j=1}^{n} (T - \lambda_j) \\
		h_2 &= \prod_{j < k} (T- (\lambda_j + \lambda_k)) \\
		&\vdots \\
		h_n &= T - \underbrace{(\lambda_1 + \dots + \lambda_n)}_{\sigma_1(\lambda_1, \dots, \lambda_n)}
	\end{align*}
	Jedes dieser $n$ Polynome ist symmetrisch in den $\lambda_i,\lambda_j$.
	Da $\sigma_1(\lambda_1,\dots,\lambda_n), \dots, \sigma_n(\lambda_1,\dots,\lambda_n) \in \QQ$ gilt, folgt mit Newtons Lemma~\ref{lemma:6.10}, dass $h_1,\dots,h_n \in \QQ[T]$.
	Sei $h = h_1 \cdots h_n$.
	Multiplizieren mit dem Hauptnenner $c_0$ der Koeffizienten liefert $c_0 h \in \ZZ[T]$, und die Nullstellen dieses Polynoms sind genau die Exponenten in \eqref{eq:6.12}.
	Schreibe:
	\begin{align*}
		c_0h 	&= c_0T^N + \dots + c_rT^{N-r} & c_j \in \ZZ, c_0,c_r \neq 0 \\
				&= g \cdot T^{N-r}
	\end{align*}
	mit $g \in \ZZ[T], \deg(g) = r \geq 1$ und $g(0) \neq 0$, also $g = c_0T^r + \dots + c_r$.
	Damit schreibt sich \eqref{eq:6.12} als
	\begin{equation}
		\exp(\beta_1) + \dots + \exp(\beta_r) + \underbrace{\exp(0) + \dots + \exp(0) + 1}_{= K \geq 1} = 0 \label{eq:6.12.2}
	\end{equation}
	mit $c_0(T-\beta_1)(T-\beta_2) \cdots (T-\beta_3) = g \in \QQ[T]$.
	
	Verfahre nun ähnlich wie im Beweis zu Theroem~\ref{thm:6.7}:
	Sei $p \in \PP$.
	\begin{align*}
		f = f_p &= c_0^s \cdot T^{p-1} \cdot \frac{1}{(p-1)!} (T-\beta_1)^p \cdots (T - \beta_r)^p & s = rp-1 \\
		F = F_p &= f + f' + f'' + \dots
	\end{align*}
	Weiter:
	\begin{align*}
		\frac{d}{dx} (\exp(-x)F(x)) &= \exp(-x)(-F(x) + F'(x)) = -\exp(-x)f(x) \\
		\Rightarrow \quad \exp(-x)F(x) - F(0) &= - \int_{0}^{x} \exp(-y)f(y) dy \\
					&= - x \int_{0}^1 \exp(-tx)f(tx) dt \\
		\Rightarrow \quad F(x9 - \exp(x)F(0) &= -x \int_{0}^{1} \exp(1-tx)f(tx)dt.
	\end{align*}
	Diese Integrale hängen von $p \in \PP$ ab, für $p \rightarrow \infty$ und festes $x$ konvergiert dieser Ausdruck gegen $0$.
	
	Betrachte nun
	\[
		\sum_{j=1}^{r} F(\beta_j) - \exp(\beta_j) \cdot F(0) \stackrel{\eqref{eq:6.12.2}}{=} \sum_{j=1}^{r} F(\beta_j) + K \cdot F(0) = \underbrace{-\sum_{j=1}^{r} \beta_j \int_{0}^{1} \exp(1-\beta_j \cdot t)f(t\beta_j) dt}_{\rightarrow 0 \text{ für } p \in \PP, p \rightarrow \infty}
	\]
	Es gilt $f^{(k)}(\beta_1) = 0$ für $0 \leq k < p$.
	Für $k \geq p$ ist $f^{(k)}(\beta_1)$ ein Produkt aus $pc_0^s$ und einem ganzzahligen Polynom in den $\beta_j$ von Grad $\leq rp-1$.
	Folglich ist $\sum_{j=1}^{r} f^{(k)}(\beta_j)$ ein Produkt aus $pc_0^s$ und einem symmetrischen ganzzahligen Polynom in den $\beta_j$.
	Seien $\wt{\sigma_1}, \dots, \wt{\sigma_r}$ die elementar symmetrischen Polynome in $r$ Variablen.
	Es folgt
	\[
		(T-\beta_1) \cdots (T-\beta_r) = T^r - \wt{\sigma_1}(\beta_1)T^{r-1} + \cdots + (-1)^r \wt{\sigma_r} (p_1,\dots,p_r) = T^r - \frac{c_1}{c_0} T^{r-1} + \dots + (-1)^r \frac{c_r}{c_0}
	\]
	und damit $\sum_{j=1}^{r} f^{(k)}(\beta_j) \in p\ZZ$ sowie $\sum_{j=1}^{r} F(\beta_j) \in p\ZZ$.
	
	Weiter gilt $f^{(k)}(0) = 0$ für $0 \leq k < p-1$, also
	\[
		f^{(p-1)}(0) = c_0^s \cdot (-\beta_1)^p \cdots (-\beta_r)^p = c_0^s \enb{\frac{c_r}{c_o}}^p = c_0^{rp-p-1} \cdot c_r^p \in \ZZ
	\]
	und $f^{(k)}(0) \in p\ZZ$ für $k \geq p$.
	Damit ist
	\[
		\sum_{j=1}^{r} F(p_j) + k \cdot F(0) \kon K \cdot c_0^{(r-1)p-1} \cdot c_r^p \modu{p}.
	\]
	Für $p > K, \abs{c_0}, \abs{c_r}$ ist diese Zahl nicht durch $p$ teilbar. \lightning
\end{beweis}

\begin{ausblick}[\enquote{Die Quadratur des Kreises ist unmöglich.}]
	\label{aus:6.13}
	Sei $\mathcal{K} \subseteq \RR^2$ die Menge aller Punkte, die sich mit Zirkel und Lineal aus Punkten mit rationalen Koordinaten konstruieren lassen. \marginnote{[13] \\ 20.7.}
	Wie sehen die Koordinaten solcher Punkte aus?
	Wenn man zwei Geraden schneidet, löst man ein lineares Gleichungssystem.
	Wenn man zwei Kreise und eine Gerade schneidet, löst man quadratische Gleichungen.
	Man kann zeigen (etwa in der Vorlesung \textit{Einführung in die Algebra}):
	Die Menge $L$ der $x$- und $y$-Koordinaten der Punkte in $\mathcal{K}$ bildet einen Körper, mit $\QQ \subseteq L \subseteq A$, wobei $A$ die Menge aller algebraischen Zahlen in $\CC$ ist.
	
	Nach Theroem~\ref{thm:6.12} gilt $\pi, e \notin L$.
	Wenn man mit Zirkel und Lineal ein Rechteck mit Seitenlänge $a,b$ mit $ab = \pi$ konstruieren könnte, so wäre $a,b \in L$ und damit $ab = \pi \in L$, was ein Widerspruch wäre.
	Das ist der Grund, warum die \enquote{Quadratur des Kreises} unmöglich ist.
\end{ausblick}
\cleardoubleoddemptypage